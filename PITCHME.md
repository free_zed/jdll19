## Générateur de site statique, et pourquoi pas?

_7 avril 2019 - pro.zind.fr - JDLL.org_

![](img/jdll.jpg)

---

### Me, myself & I


![ti99](img/ti99.jpg)

Note:

- Je m'appelle …
- Départ dynamique, lever de mains
- Qui + de 40 ans ?
- (dans qque semaines)
- découverte ordinateur tôt
- Fascination pour les outils = mécanique (discipline de la Physique)
- Évolution en parallèle Meca / Info
- études de mécanique

+++

![2007](img/2007.jpg)

Note:

- 17 ans de gestion technique
- OC en 2018, pydev Mars

---

### Comment je tombe dans la marmite (statique)  ?

![](img/2015-gh-jekyll.png)

Note:

- Jekyll / Github-pages
- Pelican avec intérêt pour py
- site pro janvier 19 (+ CI)

---

### Dis moi Jamy, comment ça marche?

**Statique** / **Dynamique**

Note:

- niveau débutant
- C'est quoi un site Internet ?

+++

**Statique** : contenu existant

par ex. :
@ul
- un article
- des CGV
- une présentation
- …
@ulend

+++

**Dynamique** : contenu généré à la demande

par ex. :
@ul
- un fil de discussion de forum
- un panier d'achat
- un retour d'information (via formulaire)
- …
@ulend

+++

**Échanges client / serveur**

+++

**Échanges client / serveur**

![](img/11.png)

+++

**Échanges client / serveur**

![](img/12.png)

+++

**Échanges client / serveur**

![](img/13.png)

+++

**Échanges client / serveur**

![](img/14.png)

+++

Zoom sur le serveur **dynamique**

![](img/20.png)

+++

Zoom sur le serveur **dynamique**

![](img/21.png)

+++

Zoom sur le serveur **dynamique**

![](img/22.png)

+++

Zoom sur le serveur **dynamique**

![](img/23.png)

+++

Zoom sur le serveur **dynamique**

![](img/24.png)

+++

Zoom sur le serveur **dynamique**

![](img/25.png)

+++

Zoom sur le serveur **dynamique**

![](img/26.png)

+++

Zoom sur le serveur **Statique**

![](img/31.png)

+++

Zoom sur le serveur **Statique**

![](img/31.png)

(et c'est tout !)

---

### Un cas concret

_(…et radical)_


Note:

- CF presentation pub LQDN d'hier

+++

https://solar.lowtechmagazine.com

![](img/ltws-1.png)

+++

### Avant

![](img/ltws-2.png)

+++

### Après

![](img/ltws-3.png)

---

@snap[east]
### Quels outils?

(_gitlab-pages trends_)
@snapend

@snap[west]
![](img/ssg-gitlab-pages.png)
@snapend

---

> Talk is cheap. Show me the code.

`pip install pelican`

![](img/pelican.png)

+++

`live coding` :

https://staticbird.gitlab.io/

---

### Bilan

+++

Points forts:

@ul
- léger
- **vraiment**!
- rapide a mettre en place
- **vraiment**!
@ulend

+++

Axe d'amélioration ©® :

@ul
- les compétences d'édition : git, gitlab, markdown, …
@ulend

mais………

+++

### getlektor.com !

![](img/lektor.png)

---

**Credits :**
- https://commons.wikimedia.org/wiki/File:TI99-IMG_1680.jpg by **Rama** [CC BY-SA 2.0 fr](https://creativecommons.org/licenses/by-sa/2.0/fr/deed.en)
- https://commons.wikimedia.org/wiki/File:Dolibarr1.svg by **Mouh2jijel** [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)

**Ressources :**
- https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website
- https://httparchive.org/reports/page-weight?start=2019_01_01&end=latest&view=grid
- http://getlektor.com/

---

@snap[north]
### Des questions ?
@snapend

@snap[west]
![QRcode](img/qrcode.png)
@snapend

@snap[south]
http://pro.zind.fr
@snapend

@snap[east]
### Merci !
@snapend
