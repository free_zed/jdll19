Générateur de site statique, et pourquoi pas ?
==============================================

Présentation faite dans le cadre des [jdll.org][jdll]
-----------------------------------------------------

Support : https://gitpitch.com/free_zed/jdll19/master?grs=gitlab

### 7 avril 2019 - Lyon

#### Pelican + Gitlab

https://staticbird.gitlab.io

* `git init staticbird.gitlab.io`
* `cd staticbird.gitlab.io`
* `git remote add origin git@gitlab.com:staticbird/staticbird.gitlab.io.git`
* `virtualenv --python python3 .venv`
* `source .venv/bin/activate`
* `pip install pelican markdown`
* `pip freeze`
* `geany requirements.txt`
* `ls`
* `pelican-quickstart`
* `ls`
* `geany pelicanconf.py`
* another example `geany ../freezed.gl.io/pelicanconf.py`
* `geany publishconf.py`
* add 1st article
```
Title: Hello world !
Date: 2019-04-07 13:30
Summary: Un premier article dans pelican
Category: News
Tags: web, python, pelican, hello
Status: Published

Les JDLL c'est chouette !
```
* `make devserver`
* add 2nd article (`pelican-installation.md`)
* `<ctrl-c>`
* `geany .gitlab-ci.yml`
```
image: python:3.6-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican -s publishconf.py -o public
  artifacts:
    paths:
    - public/
```
* `cp ../freezed.gl.io/.gitignore .`
* `git add .gitignore`
* `git add .`
* `git commit -m "Initial commit pelican hello world"`
* gitlab add `staticbird` group
* `git push origin master`
* gitlab setup :
    * https://gitlab.com/staticbird/staticbird.gitlab.io/edit
    * https://gitlab.com/staticbird/staticbird.gitlab.io/-/jobs
    * https://gitlab.com/staticbird/staticbird.gitlab.io/pages

Credits images
--------------

- https://commons.wikimedia.org/wiki/File:TI99-IMG_1680.jpg by **Rama** [CC BY-SA 2.0 fr](https://creativecommons.org/licenses/by-sa/2.0/fr/deed.en)
- https://commons.wikimedia.org/wiki/File:Dolibarr1.svg by **Mouh2jijel** [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)

Ressources
----------

- https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website
- https://httparchive.org/reports/page-weight?start=2019_01_01&end=latest&view=grid
- https://docs.getpelican.com/en/stable/
- https://gitlab.com/pages/pelican/
- http://getlektor.com/

[jdll]: http://jdll.org
[pinst]: http://docs.getpelican.com/en/3.6.3/install.html
